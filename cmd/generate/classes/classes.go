// Parses Godot documentation and Godot API to generate wrappers
// for native Godot classes this way they can be passed from an to Godot engine

package classes

import (
	"bytes"
	"fmt"
	"log"
	"path/filepath"
	"runtime"
	"sync"
	"text/template"

	"github.com/google/uuid"
)

// visited is a customary type used to don't parse on already worked classes
type visited map[uuid.UUID]struct{}

// Generate generates Godot classes wrappers using api.json definition on godot_headers
func Generate() {

	apiFilePath, err := getAPIFilePath()
	if err != nil {
		log.Fatal(err)
	}

	api := NewAPI(apiFilePath)
	if err := api.Load(); err != nil {
		log.Fatal(err)
	}

	// create a map to keep track of already visited classes and documentation
	visitedClasses := make(map[uuid.UUID]struct{})
	visitedDocs := make(map[uuid.UUID]struct{})

	// create locks for our maps
	classesLock := &sync.Mutex{}
	docsLock := &sync.Mutex{}

	// create new waiting group
	var wg sync.WaitGroup
	// wg.Add(len(api.GodotClasses))

	// generate our classes concurrently
	for uid := range api.GodotClasses {
		// go generateClass(&wg, uid, api, visitedClasses, visitedDocs, classesLock, docsLock)
		generateClass(&wg, uid, api, visitedClasses, visitedDocs, classesLock, docsLock)
		return
	}

	// wait for goroutines to finish
	// wg.Wait()
}

// generate class generates the given class UUID bindings and documentation
func generateClass(wg *sync.WaitGroup, id uuid.UUID, api *API, vClasses, vDocs visited, cLock, dLock *sync.Mutex) {

	// make sure we unlock the wait group
	// defer wg.Done()

	// retrieve class data from API
	classData := api.GodotClasses[id]

	// generate class documentation
	doc, docGenerateErr := generateClassDocumentation(classData, vDocs, dLock, api)
	if docGenerateErr != nil {
		log.Fatal(docGenerateErr)
	}
	classData.Doc = *doc

	// generate Go type struct wrapper
	classData.RealName = classData.Name
	if classData.Singleton {
		classData.RealName = classData.SingletonName
	}

	templatePath, templatePathErr := getTemplatePath("class.go")
	if templatePathErr != nil {
		log.Fatal("could not locate template path")
	}
	tpl, parseErr := template.ParseFiles(templatePath)
	if parseErr != nil {
		log.Printf("could not parse template file %s\n\t%s", templatePath, parseErr)
	}

	bufBytes := []byte{}
	buf := bytes.NewBuffer(bufBytes)

	if err := tpl.Execute(buf, &classData); err != nil {
		log.Printf("could not execute template: %s", err)
		return
	}

	fmt.Println(buf.String())
}

// get the template path
func getTemplatePath(templateType string) (string, error) {

	currentPath, err := getCurrentPath()
	if err != nil {
		return "", err
	}

	return filepath.Join(currentPath, "..", "templates", templateType+".tmpl"), nil
}

// get the current in execution file path on disk
func getCurrentPath() (string, error) {

	_, filename, _, ok := runtime.Caller(0)
	if !ok {
		return "", fmt.Errorf("could not get current file execution path")
	}

	return filepath.Dir(filename), nil
}
