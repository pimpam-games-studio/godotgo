module gitlab.com/pimpam-games-studio/godotgo

go 1.14

require (
	github.com/fatih/camelcase v1.0.0 // indirect
	github.com/google/uuid v1.1.1
	github.com/magefile/mage v1.9.0 // indirect
	github.com/pinzolo/casee v0.0.0-20191019093852-17765ba5eb57
)
