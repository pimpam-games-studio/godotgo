package classes

import (
	"encoding/xml"
	"fmt"
	"io/ioutil"
	"log"
	"path/filepath"
	"strings"
	"sync"
)

// GodotAPIMethodDocs is a customary type used with GodotAPIDoc wrapper
type GodotAPIMethodDocs []GodotAPIMethodDoc

// GodotAPIDoc is used to parse documentation from official sources
type GodotAPIDoc struct {
	Name               string             `xml:"name,attr"`
	Description        string             `xml:"description"`
	BriefDescription   string             `xml:"brief_description"`
	Methods            GodotAPIMethodDocs `xml:"methods>method"`
	GodotGoDescription string
	BaseClasses        []string
}

// get the documentation for the given method and return it as a Go comment
func (doc *GodotAPIDoc) GetMethodDocumentation(goMethod, method string) string {

	fmt.Println("MethodString:", method)
	docstring := "// no documentation"
	if method != "" {
		for _, methodValue := range doc.Methods {
			if methodValue.Name == method {
				docstring = fmt.Sprintf("/*\n%s %s\n*/", goMethod, methodValue.Description)
				break
			}
		}
	}

	return docstring
}

// GodotAPIMethodDoc is used to parse methods documentation from official sources
type GodotAPIMethodDoc struct {
	Name        string `xml:"name,attr"`
	Description string `xml:"description"`
}

// recursively iterate over all base classes and fill BaseClasses names slice
func (doc *GodotAPIDoc) fillBaseClasses(baseClass string, api *API) {

	doc.BaseClasses = append(doc.BaseClasses, baseClass)
	class, ok := api.GodotClasses[api.nameToIDMap[baseClass]]
	if !ok {
		return
	}

	if class.BaseClass != "" {
		doc.fillBaseClasses(class.BaseClass, api)
	}
}

// return back a crafted URL to the official Godot documentation
func (doc *GodotAPIDoc) officialDocUrl(classname string) string {
	return fmt.Sprintf("https://godot.readthedocs.io/en/latest/classes/class_%s.html", strings.ToLower(classname))
}

// generateClassDocumentation parses the given class documentation if any
func generateClassDocumentation(class GodotClass, visitedDocs visited, docsLock *sync.Mutex, api *API) (*GodotAPIDoc, error) {

	docsLock.Lock()
	if _, ok := visitedDocs[class.ID]; ok {
		// this doc has been already visited, skip it
		log.Printf("doc for class %s[%s] has been already visited, skipping", class.Name, class.ID.String())
	}
	docsLock.Unlock()

	hasParent := class.BaseClass != ""
	isReferenceCounted := class.IsReference || class.Name == "Reference"

	docFilesPath, pathErr := getDocClassesPath()
	if pathErr != nil {
		return nil, fmt.Errorf("could not get Godot documentation path, please run `go run build/mage get_docs`\nthe error was: %w", pathErr)
	}

	var doc GodotAPIDoc
	classDocFile := filepath.Join(docFilesPath, class.Name+".xml")
	if class.SingletonName != "" {
		classDocFile = filepath.Join(docFilesPath, class.SingletonName+".xml")
	}

	data, readErr := ioutil.ReadFile(classDocFile)
	if readErr != nil {
		log.Printf("could not find/read doc file %s, no Godot documentation will be available..\n\t%s\n", classDocFile, readErr)
	}

	if err := xml.Unmarshal(data, &doc); err != nil {
		log.Printf("could not unmarshal XML document %s\n\t%s", docFilesPath, err)
	}

	if hasParent {
		doc.fillBaseClasses(class.BaseClass, api)
	}

	switch class.Name {
	case "Reference":
		doc.GodotGoDescription = "// Base class of all Godot reference counted classes. Inherits 'Object'\n"
	case "Object":
		doc.GodotGoDescription = "// The base class for all Godot's classes\n"
	default:
		memoryModelType := "unsafe"
		if isReferenceCounted {
			memoryModelType = "managed by Godot by reference count"
		}
		inheritance := ""
		if hasParent {
			inheritance = fmt.Sprintf("inherits '%s' ", class.BaseClass)
		}

		singletonName := ""
		if class.SingletonName != "" {
			singletonName = fmt.Sprintf("%s ", class.SingletonName)
		}

		doc.GodotGoDescription = fmt.Sprintf(
			"// '%s %sclass %s' %s(%s)\n",
			class.APIType, singletonName, class.Name, inheritance, memoryModelType,
		)
	}

	if isReferenceCounted {
		doc.GodotGoDescription += `//
// Memory management
//
// Values of this type are automatically managed at
// Godot's context through reference counting`
	} else {
		doc.GodotGoDescription += `//
// Memory management:
//     Values of this type are not managed automatically
//     by Godot and the user should take care of their
//     destruction using ` + class.Name + `.Free()`
	}
	doc.GodotGoDescription += "\n//\n// For more information refer to " + doc.officialDocUrl(class.Name)

	docsLock.Lock()
	visitedDocs[class.ID] = struct{}{}
	docsLock.Unlock()

	return &doc, nil
}

func getDocClassesPath() (string, error) {

	docClassesPath, err := getDocsPath()
	if err != nil {
		return "", err
	}

	return filepath.Join(docClassesPath, "doc", "classes"), nil
}

func getDocsPath() (string, error) {

	docsPath, err := getCurrentPath()
	if err != nil {
		return "", err
	}

	return filepath.Join(docsPath, "..", "..", "..", "doc"), nil
}
