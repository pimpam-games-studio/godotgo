// Godot API wrappers

package classes

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"path/filepath"
	"strings"

	"github.com/google/uuid"
	"github.com/pinzolo/casee"
)

// GodotConstant is a customary type to use with Godot wrappers
type GodotConstant map[string]int

// GodotProperties is a customary type to use with Godot wrappers
type GodotProperties []GodotProperty

// GodotSignals is a customary type to use with Godot wrappers
type GodotSignals []GodotSignal

// GodotMethods is a customary type to use with Godot wrappers
type GodotMethods []GodotMethod

// GodotEnums is a customary type to use with Godot wrappers
type GodotEnums []GodotEnum

// GodotSignalArguments is a customary type to use with Godot wrappers
type GodotSignalArguments []GodotSignalArgument

// GodotMethodArguments is a customary type to use with Godot wrappers
type GodotMethodArguments []GodotMethodArgument

// GodotEnumValues is a customary type to use with Godot wrappers
type GodotEnumValues map[string]int

// GodotClass Go wrapper for Godot Classes based on api.json file on godot_header
type GodotClass struct {
	RealName      string
	ID            uuid.UUID       `json:"id,omitempty"`
	Name          string          `json:"name,omitempty"`
	APIType       string          `json:"api_type,omitempty"`
	BaseClass     string          `json:"base_class,omitempty"`
	SingletonName string          `json:"singleton_name,omitempty"`
	Singleton     bool            `json:"singleton,omitempty"`
	Instanciable  bool            `json:"instanciable,omitempty"`
	IsReference   bool            `json:"is_reference,omitempty"`
	Constants     GodotConstant   `json:"constants,omitempty"`
	Properties    GodotProperties `json:"properties,omitempty"`
	Signals       GodotSignals    `json:"signals,omitempty"`
	Methods       GodotMethods    `json:"methods,omitempty"`
	Enums         GodotEnums      `json:"enums,omitempty"`
	Doc           GodotAPIDoc

	propertiesOrder []uuid.UUID
}

// CamelCase gets a string and convert it to CamelCase
func (gdc *GodotClass) CamelCase(str string) string {
	return casee.ToCamelCase(str)
}

// PascalCase gets a string and convert it to PascalCase
func (gdc *GodotClass) PascalCase(str string) string {
	return casee.ToPascalCase(str)
}

// GodotProperty Go wrapper for Godot Classes based on api.json file on godot_header
type GodotProperty struct {
	ID     uuid.UUID `json:"id,omitempty"`
	Name   string    `json:"name,omitempty"`
	Type   string    `json:"type,omitempty"`
	Getter string    `json:"getter,omitempty"`
	Setter string    `json:"setter,omitempty"`
	Index  int       `json:"index,omitempty"`
}

// GodotSignal Go wrapper for Godot Classes based on api.json file on godot_header
type GodotSignal struct {
	ID        uuid.UUID
	Name      string               `json:"name"`
	Arguments GodotSignalArguments `json:"arguments"`
}

// GodotMethod Go wrapper for Godot Classes based on api.json file on godot_header
type GodotMethod struct {
	ID           uuid.UUID            `json:"id,omitempty"`
	Name         string               `json:"name,omitempty"`
	ReturnType   string               `json:"return_type,omitempty"`
	IsEditor     bool                 `json:"is_editor,omitempty"`
	IsNoScript   bool                 `json:"is_noscript,omitempty"`
	IsConst      bool                 `json:"is_const,omitempty"`
	IsReverse    bool                 `json:"is_reverse,omitempty"`
	IsVirtual    bool                 `json:"is_virtual,omitempty"`
	HasVarArgs   bool                 `json:"has_varargs,omitempty"`
	IsFromScript bool                 `json:"is_from_script,omitempty"`
	Arguments    GodotMethodArguments `json:"arguments,omitempty"`
	HasReturn    bool
	GodotAPIName string

	argumentsOrder []uuid.UUID
}

// UnmarshalJSON implements the json.Unmarshaler interface for GodotMethod
func (gdm *GodotMethod) UnmarshalJSON(data []byte) error {

	// we need an alias to do not inherit this very same method
	// so we can call json.Unmarshal on it without going into inf loop
	type Alias GodotMethod
	aux := struct {
		*Alias
		Name       string `json:"name"`
		ReturnType string `json:"return_type"`
	}{
		Alias: (*Alias)(gdm),
	}

	if err := json.Unmarshal(data, &aux); err != nil {
		return err
	}

	gdm.GodotAPIName = aux.Name
	gdm.Name = gdm.parseMethodName(aux.Name)
	gdm.ReturnType = gdm.parseReturnType(aux.ReturnType)

	return nil
}

// parseMethodName parses the method name to make it Go idiomatic
func (gdm *GodotMethod) parseMethodName(name string) string {

	fn := casee.ToPascalCase
	if name[0] == '_' {
		fn = casee.ToCamelCase
	}

	return fn(strings.Replace(name, "_", "", 1))
}

// parseReturnType parses the return type to convert them to gdnative-go types
func (gdm *GodotMethod) parseReturnType(returntype string) string {

	// to get a list of all possible return types defined in the API you can
	// run this command on any UNIX like Operating System or on WSL on Windows:
	//	grep -r "return_type" godot_headers/api.json \
	//		| awk '{print $3}' | sort | uniq | sed 's/[",]//g'
	//
	gdm.HasReturn = true

	if returntype == "void" {
		gdm.HasReturn = false
		return ""
	}

	if len(returntype) > 4 && returntype[:4] == "enum" {
		return fmt.Sprintf("gdnative.%s", strings.Replace(returntype[5:], "::", "", 1))
	}

	if returntype == "float" {
		return "real"
	}

	return fmt.Sprintf("gdnative.%s", casee.ToPascalCase(returntype))
}

// GodotEnum Go wrapper for Godot Classes based on api.json file on godot_header
type GodotEnum struct {
	ID     uuid.UUID       `json:"id,omitempty"`
	Name   string          `json:"name,omitempty"`
	Values GodotEnumValues `json:"values,omitempty"`
}

// GodotSignalArgument Go wrapper for Godot Classes based on api.json file on godot_header
type GodotSignalArgument struct {
	ID           uuid.UUID `json:"id,omitempty"`
	Name         string    `json:"name,omitempty"`
	DefaultValue string    `json:"default_value,omitempty"`
	Type         string    `json:"type,omitempty"`
}

// GodotMethodArgument Go wrapper for Godot Classes based on api.json file on godot_header
type GodotMethodArgument struct {
	ID              uuid.Domain `json:"id,omitempty"`
	HasDefaultValue bool        `json:"has_default_value,omitempty"`
	DefaultValue    string      `json:"default_value,omitempty"`
	Name            string      `json:"name,omitempty"`
	Type            string      `json:"type,omitempty"`
}

// API is a wrapper around api.json file operations
type API struct {
	jsonFilePath string
	GodotClasses map[uuid.UUID]GodotClass
	nameToIDMap  map[string]uuid.UUID
}

// NewAPI creates a new API value and returns a pointer value to its address
func NewAPI(APIFilePath string) *API {

	api := API{
		jsonFilePath: APIFilePath,
		GodotClasses: map[uuid.UUID]GodotClass{},
		nameToIDMap:  map[string]uuid.UUID{},
	}

	return &api
}

// Load loads the API file from godot_headers and unmarshal it into Go structures
func (api *API) Load() error {

	if api.jsonFilePath == "" {
		return fmt.Errorf("json file path is not set, please set it up")
	}

	data, readErr := ioutil.ReadFile(api.jsonFilePath)
	if readErr != nil {
		return fmt.Errorf("could not open %s JSON API definition file: %w", api.jsonFilePath, readErr)
	}

	var classes []GodotClass
	err := json.Unmarshal(data, &classes)
	if err != nil {
		return fmt.Errorf("could not unmarshal data from %s JSON API definition file:\n\t%w", api.jsonFilePath, err)
	}

	for _, class := range classes {
		class.ID = uuid.Must(uuid.NewRandom())
		api.GodotClasses[class.ID] = class
		api.nameToIDMap[class.Name] = class.ID
	}

	return nil
}

// constructs and returns the api.json file path from godot_headers
func getAPIFilePath() (string, error) {

	currentFilePath, err := getCurrentPath()
	if err != nil {
		return "", err
	}

	return filepath.Join(currentFilePath, "..", "..", "..", "godot_headers", "api.json"), nil
}
